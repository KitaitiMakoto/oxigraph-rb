RUST_TARGET = "target/release/liboxigraph_rb.so"
RUST_SRC = FileList["src/**/*.rs"]

RUST_SRC.each do |path|
  file path
end

file RUST_TARGET => RUST_SRC do
  sh "cargo build --release"
end

desc "Build extension library"
task extlib: RUST_TARGET
