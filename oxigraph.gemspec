require "tomlrb"

Gem::Specification.new do |spec|
  spec.name          = "oxigraph"
  spec.version       = Tomlrb.load_file("Cargo.toml")["package"]["version"]
  spec.license       = "AGPL-3.0-or-later"
  spec.authors       = ["Kitaiti Makoto"]
  spec.email         = ["KitaitiMakoto@gmail.com"]

  spec.summary       = %q{Binding for Oxigraph, a SPARQL graph database}
  spec.description   = %q{Binding for library of Oxigraph, a SPARQL graph database.}
  spec.homepage      = "https://gitlab.com/KitaitiMakoto/oxigraph-rb"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "#{spec.homepage}/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0")
  end

  spec.extensions    << "ext/Rakefile"

  spec.add_runtime_dependency "rutie"

  spec.add_development_dependency "test-unit"
  spec.add_development_dependency "tomlrb"
  spec.add_development_dependency "rubygems-tasks"
  spec.add_development_dependency "yard"
end
