extern crate lazy_static;

use oxigraph::{
    self,
    io::{read::DatasetParser, DatasetFormat},
    sparql::{QueryOptions, QueryResults},
};
use rutie::{
    class, methods, wrappable_struct, AnyObject, Array, Boolean, Class, Hash, Integer, Module, NilClass, Object, RString, Symbol, VM,
    rubysys::value::ValueType,
};
use std::io::Cursor;

macro_rules! def_length {
    ($rtself:ident , $wrapper_name:ident) => {
        Integer::new($rtself.get_data(&*$wrapper_name).len() as i64)
    }
}

macro_rules! def_each {
    ($rtself:ident , $wrapper_name:ident) => {
        {
            if VM::is_block_given() {
                let store = $rtself.get_data(&*$wrapper_name);
                for quad in store.iter() {
                    let quad = quad.map_err(|e| VM::raise(Class::from_existing("IOError"), &e.to_string())).unwrap();
                    let rstring = RString::new_utf8(quad.to_string().as_str());
                    VM::block_proc().call(&[rstring.to_any_object()]);
                }
            } else {
                unimplemented!();
            }

            $rtself
        }
    }
}

// TODO: Raise custom syntax error instead of ArgumentError for syntax error
macro_rules! def_insert_from_quad_string {
    ($rtself:ident , $wrapper_name:ident, $quad:ident) => {
        {
            let quad_string = $quad.map_err(|e| VM::raise_ex(e)).unwrap();
            let quad_str = quad_string.to_str();
            let parser = DatasetParser::from_format(DatasetFormat::NQuads);
            let quad_reader = parser
                .read_quads(Cursor::new(quad_str))
                .expect("unreachable");
            let quads = quad_reader
                .collect::<Result<Vec<_>, _>>()
                .map_err(|e| VM::raise(Class::from_existing("ArgumentError"), &e.to_string()))
                .unwrap();
            let store = $rtself.get_data(&*$wrapper_name);
            for mut q in quads.iter() {
                // TODO: handle error
                let _ = store.insert(q.as_ref());
            }

            $rtself
        }
    }
}

macro_rules! def_remove_by_quads_string {
    ($rtself:ident , $wrapper_name:ident , $quad:ident) => {
        {
            let quad_string = $quad.map_err(|e| VM::raise_ex(e)).unwrap();
            let quad_str = quad_string.to_str();
            let parser = DatasetParser::from_format(DatasetFormat::NQuads);
            let quad_reader = parser
                .read_quads(Cursor::new(quad_str))
                .expect("unreachable");
            let quads = quad_reader
                .collect::<Result<Vec<_>, _>>()
                .map_err(|e| VM::raise(Class::from_existing("ArgumentError"), &e.to_string()))
                .unwrap();
            let store = $rtself.get_data(&*$wrapper_name);
            for mut q in quads.iter() {
                // TODO: handle error
                let _ = store.remove(q.as_ref());
            }

            $rtself
        }
    }
}

macro_rules! def_load_dataset {
    ($rtself:ident , $wrapper_name:ident , $dataset:ident , $format:ident , $base_iri:ident) => {
        {
            let format = match $format.map_err(|e| VM::raise_ex(e)).unwrap().to_str() {
                "NQuads" => DatasetFormat::NQuads,
                "TriG" => DatasetFormat::TriG,
                _ => {
                    VM::raise(Class::from_existing("ArgumentError"), "format must be :NQuads or :TriG");
                    return $rtself
                }
            };
            let dataset = $dataset.map_err(|e| VM::raise_ex(e)).unwrap();
            let dataset_string = dataset.try_convert_to::<RString>()
                .or_else(|_| {
                    if dataset.respond_to("read") {
                        let res = unsafe {
                            dataset.send("read", &[])
                        };
                        Ok(res.try_convert_to::<RString>().unwrap())
                    } else {
                        Err("dataset must be String or respond to `read'")
                    }
                })
                .map_err(|e| VM::raise(Class::from_existing("ArgumentError"), e))
                .unwrap();
            let base_iri = $base_iri.unwrap_or(NilClass::default().to_any_object());
            let store = $rtself.get_data(&*$wrapper_name);
            let res = match base_iri.ty() {
                ValueType::RString => {
                    store.load_dataset(dataset_string.to_bytes_unchecked(), format, Some(base_iri.try_convert_to::<RString>().unwrap().to_str()))
                },
                ValueType::Nil => {
                    store.load_dataset(dataset_string.to_bytes_unchecked(), format, None)
                },
                _ => {
                    VM::raise(Class::from_existing("ArgumentError"), "base_iri must be String");
                    return $rtself
                }
            };
            res
                .map_err(|e| {
                    VM::raise(Class::from_existing("ArgumentError"), &e.to_string())
                }) // TODO: Consider proper error type
                .unwrap();
            $rtself
        }
    }
}

// TODO: Raise custom syntax error instead of ArgumentError for syntax error
macro_rules! def_query {
    ($rtself:ident , $wrapper_name:ident , $query:ident) => {
        {
            let query_string = $query.map_err(|e| VM::raise_ex(e)).unwrap();
            let query_str = query_string.to_str();
            let store = $rtself.get_data(&*$wrapper_name);
            let results = store
                .query(query_str, QueryOptions::default())
                .map_err(|e| VM::raise(Class::from_existing("ArgumentError"), &e.to_string()))
                .unwrap();

            match results {
                QueryResults::Solutions(solutions) => {
                    let mut ary = Array::new();
                    for solution in solutions {
                        let sol = solution.unwrap();
                        let mut hash = Hash::new();
                        for (v, t) in sol.iter() {
                            hash.store(RString::new_utf8(v.as_str()), RString::new_utf8(t.to_string().as_str()));
                        }
                        ary.push(hash);
                    }
                    ary.into()
                },
                QueryResults::Boolean(bool) => {
                    Boolean::new(bool).into()
                },
                QueryResults::Graph(triples) => {
                    let mut ary = Array::new();
                    for triple in triples {
                        ary.push(RString::new_utf8(&triple.unwrap().to_string()));
                    }
                    ary.into()
                }
            }
        }
    }
}

wrappable_struct!(
    oxigraph::MemoryStore,
    MemoryStoreWrapper,
    MEMORY_STORE_WRAPPER
);

class!(MemoryStore);

methods!(
    MemoryStore,
    itself,

    fn memory_store_new() -> AnyObject {
        let store = oxigraph::MemoryStore::new();
        Module::from_existing("Oxigraph")
            .get_nested_class("MemoryStore")
            .wrap_data(store, &*MEMORY_STORE_WRAPPER)
    }

    fn memory_store_length() -> Integer {
        def_length!(itself, MEMORY_STORE_WRAPPER)
    }

    fn memory_store_each() -> MemoryStore {
        if VM::is_block_given() {
            let store = itself.get_data(&*MEMORY_STORE_WRAPPER);
            for quad in store.iter() {
                let rstring = RString::new_utf8(quad.to_string().as_str());
                VM::block_proc().call(&[rstring.to_any_object()]);
            }
        } else {
            unimplemented!();
        }

       itself
    }

    fn memory_store_insert_from_quads_string(quad: RString) -> MemoryStore {
        def_insert_from_quad_string!(itself, MEMORY_STORE_WRAPPER, quad)
    }

    fn memory_store_remove_by_quads_string(quad: RString) -> MemoryStore {
        def_remove_by_quads_string!(itself, MEMORY_STORE_WRAPPER, quad)
    }

    fn memory_store_load_dataset(dataset: AnyObject, format: Symbol, base_iri: AnyObject) -> MemoryStore {
        def_load_dataset!(itself, MEMORY_STORE_WRAPPER, dataset, format, base_iri)
    }

    fn memory_store_query(query: RString) -> AnyObject {
        def_query!(itself, MEMORY_STORE_WRAPPER, query)
    }
);

wrappable_struct!(
    oxigraph::RocksDbStore,
    RocksDbStoreWrapper,
    ROCKS_DB_STORE_WRAPPER
);

class!(RocksDbStore);

methods!(
    RocksDbStore,
    itself,

    fn rocks_db_store_open(path: RString) -> AnyObject {
        let path_string = path.map_err(VM::raise_ex).unwrap();
        let path_str = path_string.to_str();
        let store = oxigraph::RocksDbStore::open(path_str)
            .map_err(|e| VM::raise(Class::from_existing("ArgumentError"), &e.to_string()))
            .unwrap();
        Module::from_existing("Oxigraph")
            .get_nested_class("RocksDbStore")
            .wrap_data(store, &*ROCKS_DB_STORE_WRAPPER)
    }

    fn rocks_db_store_length() -> Integer {
        def_length!(itself, ROCKS_DB_STORE_WRAPPER)
    }

    fn rocks_db_store_each() -> RocksDbStore {
        def_each!(itself, ROCKS_DB_STORE_WRAPPER)
    }

    fn rocks_db_store_insert_from_quads_string(quad: RString) -> RocksDbStore {
        def_insert_from_quad_string!(itself, ROCKS_DB_STORE_WRAPPER, quad)
    }

    fn rocks_db_store_remove_by_quads_string(quad: RString) -> RocksDbStore {
        def_remove_by_quads_string!(itself, ROCKS_DB_STORE_WRAPPER, quad)
    }

    fn rocks_db_store_load_dataset(dataset: AnyObject, format: Symbol, base_iri: AnyObject) -> RocksDbStore {
        def_load_dataset!(itself, ROCKS_DB_STORE_WRAPPER, dataset, format, base_iri)
    }

    fn rocks_db_store_query(query: RString) -> AnyObject {
        def_query!(itself, ROCKS_DB_STORE_WRAPPER, query)
    }
);

wrappable_struct!(
    oxigraph::SledStore,
    SledStoreWrapper,
    SLED_STORE_WRAPPER
);

class!(SledStore);

methods!(
    SledStore,
    itself,

    fn sled_store_open(path: RString) -> AnyObject {
        let path_string = path.map_err(VM::raise_ex).unwrap();
        let path_str = path_string.to_str();
        let store = oxigraph::SledStore::open(path_str)
            .map_err(|e| VM::raise(Class::from_existing("ArgumentError"), &e.to_string()))
            .unwrap();
        Module::from_existing("Oxigraph")
            .get_nested_class("SledStore")
            .wrap_data(store, &*SLED_STORE_WRAPPER)
    }

    fn sled_store_length() -> Integer {
        def_length!(itself, SLED_STORE_WRAPPER)
    }

    fn sled_store_each() -> SledStore {
        def_each!(itself, SLED_STORE_WRAPPER)
    }

    fn sled_store_insert_from_quads_string(quad: RString) -> SledStore {
        def_insert_from_quad_string!(itself, SLED_STORE_WRAPPER, quad)
    }

    fn sled_store_remove_by_quads_string(quad: RString) -> SledStore {
        def_remove_by_quads_string!(itself, SLED_STORE_WRAPPER, quad)
    }

    fn sled_store_load_dataset(dataset: AnyObject, format: Symbol, base_iri: AnyObject) -> SledStore {
        def_load_dataset!(itself, SLED_STORE_WRAPPER, dataset, format, base_iri)
    }

    fn sled_store_query(query: RString) -> AnyObject {
        def_query!(itself, SLED_STORE_WRAPPER, query)
    }
);

#[allow(non_snake_case)]
#[no_mangle]
pub extern "C" fn Init_oxigraph() {
    Module::new("Oxigraph").define(|itself| {
        itself
            .define_nested_class("MemoryStore", None)
            .define(|itself| {
                itself.def_self("new", memory_store_new);
                itself.def("length", memory_store_length);
                itself.def("each", memory_store_each);
                itself.def(
                    "insert_from_quads_string",
                    memory_store_insert_from_quads_string,
                );
                itself.def(
                    "remove_by_quads_string",
                    memory_store_remove_by_quads_string
                );
                itself.def("load_dataset", memory_store_load_dataset);
                itself.def("query", memory_store_query);
            });
        itself
            .define_nested_class("RocksDbStore", None)
            .define(|itself| {
                itself.def_self("new", rocks_db_store_open);
                itself.def("length", rocks_db_store_length);
                itself.def("each", rocks_db_store_each);
                itself.def(
                    "insert_from_quads_string",
                    rocks_db_store_insert_from_quads_string,
                );
                itself.def(
                    "remove_by_quads_string",
                    rocks_db_store_remove_by_quads_string,
                );
                itself.def("load_dataset", rocks_db_store_load_dataset);
                itself.def("query", rocks_db_store_query);
            });
        itself
            .define_nested_class("SledStore", None)
            .define(|itself| {
                itself.def_self("new", sled_store_open);
                itself.def("length", sled_store_length);
                itself.def("each", sled_store_each);
                itself.def(
                    "insert_from_quads_string",
                    sled_store_insert_from_quads_string,
                );
                itself.def(
                    "remove_by_quads_string",
                    sled_store_remove_by_quads_string,
                );
                itself.def("load_dataset", sled_store_load_dataset);
                itself.def("query", sled_store_query);
            });
    });
}
