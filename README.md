# Oxigraph

Binding for library of [Oxigraph][], a SPARQL graph database.

[Oxigraph]: https://github.com/oxigraph/oxigraph

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'oxigraph'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install oxigraph

## Usage

```ruby
require "oxigraph"

store = Oxigraph::MemoryStore.new
store.load_dataset "<http://one.example/subject1> <http://one.example/predicate1> <http://one.example/object1> <http://example.org/graph3> .", :NQuads
store.query("SELECT ?s ?p ?o FROM <http://example.com> WHERE { ?s ?p ?o }")
# => [{"s"=>"<http://one.example/subject1>",
#      "p"=>"<http://one.example/predicate1>",
#      "o"=>"<http://one.example/object1>"}]
```

## Requirements

Rust build environment.

For Unix, run

    % curl https://sh.rustup.rs -sSf | sh

For Windows, download and run [installer][].

See [Rust official installation page][] for details.

[installer]: https://static.rust-lang.org/rustup/dist/i686-pc-windows-gnu/rustup-init.exe
[Rust official installation page]: https://www.rust-lang.org/tools/install

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/KitaitiMakoto/oxigraph-rb.
