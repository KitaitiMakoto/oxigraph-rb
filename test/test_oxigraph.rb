require_relative "helper"

class TestOxigraph < Test::Unit::TestCase
  module StoreTest
    def test_length
      assert_equal 0, @store.length
    end

    def test_each
      quad_string = "<http://one.example/subject1> <http://one.example/predicate1> <http://one.example/object1> <http://example.org/graph3> . # comments here"
      @store.insert_from_quads_string(quad_string)
      @store.each do |quad|
        assert_equal "<http://one.example/subject1> <http://one.example/predicate1> <http://one.example/object1> <http://example.org/graph3> .", quad
      end
    end

    def test_insert_from_quads_string
      quad_string = "<http://one.example/subject1> <http://one.example/predicate1> <http://one.example/object1> <http://example.org/graph3> . # comments here"
      @store.insert_from_quads_string(quad_string)

      assert_equal 1, @store.length

      assert_raise TypeError do
        @store.insert_from_quads_string(1)
      end

      assert_raise_message /unexpected character/ do
        @store.insert_from_quads_string("invalid turtle")
      end

      assert_raise_message /not found/ do
        @store.insert_from_quads_string
      end
    end

    def test_load_dataset
      assert_raise ArgumentError do
        @store.load_dataset "<http://example.com> <http://example.com> <http://example.com> <http://example.com> .", :InvalidFormat
      end

      assert_raise ArgumentError do
        @store.load_dataset "<http://example.com> <http://example.com> <http://example.com> <http://example.com> ."
      end

      @store.load_dataset "<http://example.com> <http://example.com> <http://example.com> <http://example.com> .", :NQuads
      assert_equal 1, @store.length

      @store.load_dataset "<http://example.com> <http://example.com> <http://example.com> <http://example.com> .", :NQuads
      assert_equal 1, @store.length

      assert_raise ArgumentError do
        @store.load_dataset "Invalid dataset", :NQuads
      end

      @store.load_dataset File.open("test/fixtures/dataset.nq"), :NQuads
      assert_equal 2, @store.length

      assert_raise ArgumentError do
        @store.load_dataset File.open("Rakefile"), :NQuads
      end
    end

    def test_load_dataset_with_base_iri
      @store.load_dataset "<http://example.com> <http://example.com> <http://example.com> <http://example.com> .", :NQuads, "http://example.com"
      assert_equal 1, @store.length
    end

    def test_remove
      @store.load_dataset "<http://one.example/subject1> <http://one.example/predicate1> <http://one.example/object1> <http://example.org/graph3> .", :NQuads

      @store.remove_by_quads_string "<http://one.example/subject1> <http://one.example/predicate1> <http://one.example/object1> <http://example.org/graph3> ."
      assert_equal 0, @store.length
    end

    def test_query
      quad_string = "<http://one.example/subject1> <http://one.example/predicate1> <http://one.example/object1> <http://example.org/graph3> . # comments here"
      @store.insert_from_quads_string(quad_string)

      res = @store.query("SELECT ?s ?p ?o FROM <http://example.org/graph3> WHERE { ?s ?p ?o }")
      assert_equal [{"s" => "<http://one.example/subject1>", "p" => "<http://one.example/predicate1>", "o" => "<http://one.example/object1>"}], res

      assert_raise ArgumentError do
        @store.query("INVALID SPARQL QUERY");
      end

      assert_raise TypeError do
        @store.query(3);
      end
    end
  end

  class TestMemoryStore < self
    include StoreTest

    def setup
      @store = Oxigraph::MemoryStore.new
    end
  end

  class TestRocksDbStore < self
    include StoreTest

    def setup
      @db_dir = Dir.mktmpdir
      @store = Oxigraph::RocksDbStore.new(@db_dir)
    end

    def cleanup
      FileUtils.rmtree @db_dir
    end

    def test_open
      Dir.mktmpdir do |dir|
        store = Oxigraph::RocksDbStore.new(dir)
        assert_instance_of Oxigraph::RocksDbStore, store
      end
    end
  end

  class TestSledStoer < self
    include StoreTest

    def setup
      @db_dir = Dir.mktmpdir
      @store = Oxigraph::SledStore.new(@db_dir)
    end

    def cleanup
      FileUtils.rmtree @db_dir
    end
  end
end
